//
// Created by lex on 11-2-18.
//

#ifndef SUDOKU_READER_HPP
#define SUDOKU_READER_HPP

#include <tuple>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <iterator>

#include "sudoku.hpp"

class reader {
public:
    reader(std::string path)
            : path_(std::move(path)) {}

    /**
     * Read the grids from the source file given in the
     * constructor.
     *
     * @return
     */
    bool read_grids() {
        std::ifstream file(path_);

        grid g{};
        std::string title;

        int row = 0;
        std::string line;

        while (std::getline(file, line)) {
            // We don't care about empty lines.
            if (line.empty()) {
                continue;
            }

            // Comments
            if (line[0] == '#') {
                continue;
            }

            // Title
            if (line[0] == ':') {
                title = line.substr(1);
                continue;
            }

            // Sudoku row
            const auto number_row = read_sudoku_row(line);
            for (size_t i = 0; i < number_row.size(); i++) {
                g[row][i] = number_row[i];
            }

            if (row++ == 8) {
                auto tuple = std::make_tuple(title, g);
                grids_.push_back(tuple);

                row = 0;
                title = "";
            }
        }

        return true;
    }

    bool has_next_grid() {
        return current_grid_ < grids_.size();
    }

    auto get_grid_title() {
        return std::get<0>(grids_[current_grid_]);
    }

    auto get_next_grid() {
        auto g = std::get<1>(grids_[current_grid_]);
        current_grid_++;
        return g;
    }

protected:
    std::vector<int> read_sudoku_row(const std::string& line) {
        std::istringstream ss(line);
        std::vector<int> parts;

        do {
            std::string part;
            ss >> part;

            if (!part.empty()) {
                parts.push_back(std::stoi(part));
            }
        } while (ss);

        assert(parts.size() == 9);

        return parts;
    }

    size_t current_grid_ = 0;
    std::string path_;
    std::vector<std::tuple<std::string, grid>> grids_;
};

#endif //SUDOKU_READER_HPP
