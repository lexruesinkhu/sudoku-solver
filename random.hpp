//
// Created by lex on 11-2-18.
//

#ifndef SUDOKU_RANDOM_HPP
#define SUDOKU_RANDOM_HPP

#include <random>
#include <iterator>

template<typename iter, typename random_generator>
iter select_randomly(iter start, iter end, random_generator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename iter>
iter select_randomly(iter start, iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

#endif //SUDOKU_RANDOM_HPP
