#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <cmath>
#include <chrono>
#include <vector>

#include "sudoku.hpp"
#include "reader.hpp"

/**
 * Helper function for creating a row divider.
 */
void print_row_divider() {
    std::cout << '+';

    for (int i = 0; i < 18; i++) {
        if (i % 2 == 0) {
            std::cout << "---";
        } else {
            std::cout << '+';
        }
    }

    std::cout << std::endl;
}

/**
 * Helper function that prints the given grid
 * to stdout.
 *
 * @param g
 */
void print_grid(grid const& g) {
    print_row_divider();

    for (const auto &row : g) {
        std::cout << '|';

        for (const auto &col : row) {
            auto c = col == 0 ? " " : std::to_string(col);
            std::cout << ' ' << c << " |";
        }

        std::cout << std::endl;
        print_row_divider();
    }
}

/**
 * Sudoku solver program. Takes a optional argument specifying a Sudoku source file.
 * If the file is not given, the program will search for 'sudoku.txt' in the current working
 * directory.
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char* argv[]) {
    std::cout << "This program was made by Lex Ruesink." << std::endl;

    std::string filename;

    if (argc < 2) {
        std::cout << "No filename given, looking for sudoku.txt in the current directory..." << std::endl;
        filename = "./sudoku.txt";
    } else {
        filename = argv[1];
    }

    /*
     * The reader will provide all the Sudoku's that are defined
     * in the source file.
     */
    reader reader(filename);
    if (!reader.read_grids()) {
        std::cerr << "Error occurred while reading grids from file." << std::endl;
        return EXIT_FAILURE;
    }

    // Work over the Sudoku's one by one
    while (reader.has_next_grid()) {
        auto title = reader.get_grid_title();
        std::cout << "Starting on the grid '" << title << "':" << std::endl;
        auto grid = reader.get_next_grid();

        print_grid(grid);

        sudoku s(grid);

        auto start = std::chrono::high_resolution_clock::now();
        bool solved = s.solve();
        auto end = std::chrono::high_resolution_clock::now();
        auto milliseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(end - start);

        if (solved) {
            std::cout << "Solved grid '" << title << "':" << std::endl;
            print_grid(s.get_grid());
        } else {
            std::cout << "Could not find solution for grid '" << title << "'!" << std::endl;
        }

        std::cout << std::fixed
                  << "Solving algorithm took " << milliseconds.count()
                  << " microseconds (" << seconds.count()
                  << " seconds) for '" << title << "'\n" << std::endl;
    }

    return EXIT_SUCCESS;
}