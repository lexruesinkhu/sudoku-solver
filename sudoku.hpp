//
// Created by lex on 11-2-18.
//

#ifndef SUDOKU_SUDOKU_HPP
#define SUDOKU_SUDOKU_HPP

#include <cassert>
#include <iostream>
#include <array>
#include <tuple>
#include <bitset>

#include "random.hpp"

#define UNASSIGNED 0
#define CHECK_BOUNDS 0

using grid = std::array<std::array<size_t, 9>, 9>;

class sudoku {
public:
    sudoku(grid g) : grid_(g) {}

    bool solve() {
        prefill_bitsets(grid_);
        return recursive_solve(grid_);
    }

    grid const &get_grid() {
        return grid_;
    }

protected:
    // Bitsets for quickly checking valid values.
    /*
     * These bitsets are used to quickly check if a
     * given number is valid in the row, column or nonet.
     *
     * This system incurs a small performance penality on (very) easy Sudoku's
     * but helps enormously in a worst-case scenario for the brute-force algorithm.
     */
    std::array<std::bitset<9>, 9> row_bitsets;
    std::array<std::bitset<9>, 9> col_bitsets;
    std::array<std::array<std::bitset<9>, 3>, 3> nonet_bitsets;

    grid grid_;

    /*
     * This flag indicates whether we need to get
     * the starting cell.
     */
    bool started = false;

    /**
     * Before we can start solving the Sudoku,
     * the bitsets must be pre-filled with the given grid.
     *
     * @param g
     */
    void prefill_bitsets(grid &g) {
        for (size_t row = 0; row < 9; row++) {
            for (size_t col = 0; col < 9; col++) {
                if (g[row][col] == UNASSIGNED) {
                    continue;
                }

                set_bit(row, col, g[row][col] - 1, true);
            }
        }
    }

    /**
     * This function sets a bit for a given column and row
     * in the row, column and the nonet.
     *
     * @param row
     * @param col
     * @param n
     * @param state
     */
    void set_bit(size_t row, size_t col, size_t n, bool state) {
#if CHECK_BOUNDS
        assert(row < 9);
        assert(col < 9);

        row_bitsets[row].set(n, state);
        col_bitsets[col].set(n, state);

        nonet_bitsets[row / 3][col / 3].set(n, state);
#else
        row_bitsets[row][n] = state;
        col_bitsets[col][n] = state;

        nonet_bitsets[row / 3][col / 3][n] = state;
#endif
    }

    bool recursive_solve(grid &g) {
        size_t row = 0, col = 0;

        if (!started) {
            get_starting_location(g, row, col);
            started = true;
        } else {
            /*
            * If we can't find an unassigned location,
            * the sudoku is solved!
            */
            if (!find_unassigned_location(g, row, col)) {
                return true;
            }
        }

        // Search for a valid number in the unassigned position.
        for (size_t n = 1; n <= 9; n++) {
            if (valid(row, col, n)) {
                // Temporary assignment;
                g[row - 1][col - 1] = n;
                set_bit(row - 1, col - 1, n - 1, true);

                // Try to solve this path
                if (recursive_solve(g)) {
                    return true;
                }

                // Try again
                g[row - 1][col - 1] = UNASSIGNED;
                set_bit(row - 1, col - 1, n - 1, false);
            }
        }

        return false;
    }

    bool valid(size_t row, size_t col, size_t n) const {
        return valid_in_row(row, n)
               && valid_in_column(col, n)
               && valid_in_group(row, col, n);
    }

    /**
     * Search for the first unassigned location.
     *
     * @param g
     * @param row
     * @param col
     * @return
     */
    bool find_unassigned_location(grid &g, size_t &row, size_t &col) const {
        for (size_t r = 1; r <= 9; r++) {
            for (size_t c = 1; c <= 9; c++) {
                if (g[r - 1][c - 1] == UNASSIGNED) {
                    row = r;
                    col = c;

                    return true;
                }
            }
        }

        return false;
    };

    /**
     * Find the starting location for the algorithm
     * by randomly selecting a free location.
     *
     * This system incurs a slight performance hit on easy Sudoku's,
     * but saves a lot of time on Sudoku's that are well-versed against
     * a brute-force solution.
     *
     * @param g
     * @param row
     * @param col
     */
    void get_starting_location(grid &g, size_t &row, size_t &col) const {
        std::vector<std::tuple<size_t, size_t>> unassigned_cells;

        for (size_t r = 1; r <= 9; r++) {
            for (size_t c = 1; c <= 9; c++) {
                if (g[r - 1][c - 1] == UNASSIGNED) {
                    unassigned_cells.emplace_back(r, c);
                }
            }
        }

        if (unassigned_cells.empty()) {
            return;
        }

        const auto pair = *(select_randomly(unassigned_cells.begin(), unassigned_cells.end()));
        row = std::get<0>(pair);
        col = std::get<1>(pair);
    };

    bool valid_in_group(size_t row, size_t col, size_t n) const {
#if CHECK_BOUNDS
        assert(row > 0 && col <= 9);
        assert(col > 0 && col <= 9);

        return !nonet_bitsets[(row - 1) / 3][(col - 1) / 3].test(n - 1);
#else
        return !nonet_bitsets[(row - 1) / 3][(col - 1) / 3][n - 1];
#endif
    }

    bool valid_in_row(size_t row, size_t n) const {
#if CHECK_BOUNDS
        assert(row > 0 && row <= 9);
        assert(n != 0);

        return !row_bitsets[row - 1].test(n - 1);
#else
        return !row_bitsets[row - 1][n - 1];
#endif

    }

    bool valid_in_column(size_t col, size_t n) const {
#if CHECK_BOUNDS
        assert(col > 0 && col <= 9);
        assert(n != 0);

        return !col_bitsets[col - 1].test(n - 1);
#else
        return !col_bitsets[col - 1][n - 1];
#endif
    }
};

#endif //SUDOKU_SUDOKU_HPP
